using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{
    class FileLogger : ILogger
    {
        private string filePath;
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }

        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }

        public FileLogger(string path)
        {
            this.filePath = path;
        }
    }
}