using System;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            Random randNumber = new Random();
           
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6, randNumber));
            }
            diceRoller.RollAllDice();
            diceRoller.GetRollingResults();

            foreach (int result in diceRoller.ResultForEachRoll)
            {
                Console.WriteLine(result);
            }
        }
    }
}
