using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{
    class ConsoleLogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }

        public void Log(ILogable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
